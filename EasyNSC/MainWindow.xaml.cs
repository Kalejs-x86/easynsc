﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasyNSC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Credits to https://stackoverflow.com/users/360211/weston & https://stackoverflow.com/users/40347/dirk-vollmar
        // Snippet copied from https://stackoverflow.com/questions/923771/quickest-way-to-convert-a-base-10-number-to-any-base-in-net/10981113
        public static string IntToStringFast(long value, char[] baseChars)
        {
            int i = 64;
            char[] buffer = new char[i];
            int targetBase = baseChars.Length;

            do
            {
                buffer[--i] = baseChars[value % targetBase];
                value = value / targetBase;
            }
            while (value > 0);

            char[] result = new char[64 - i];
            Array.Copy(buffer, i, result, 0, 64 - i);

            return new string(result);
        }

        enum NumericBase{
            Base16,
            Base10,
            Base8,
            Base2
        }

        public static readonly char[] Base16Char = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        public static readonly char[] Base8Char = { '0', '1', '2', '3', '4', '5', '6', '7' };
        public static readonly char[] Base2Char = { '0', '1' };

        private static string ConvertToBase(long Value, NumericBase Base)
        {
            string Result = "";
            switch (Base)
            {
                case NumericBase.Base16:
                    Result = IntToStringFast(Value, Base16Char);
                    break;
                case NumericBase.Base10:
                    Result = Value.ToString();
                    break;
                case NumericBase.Base8:
                    Result = IntToStringFast(Value, Base8Char);
                    break;
                case NumericBase.Base2:
                    Result = IntToStringFast(Value, Base2Char);
                    break;
            }
            return Result;
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Base16Box_KeyUp(object sender, KeyEventArgs e)
        {
            string Input = Base16Box.Text;
            try { 
                Int64 Value = Convert.ToInt64(Input, 16);
                Base10Box.Text = ConvertToBase(Value, NumericBase.Base10);
                Base8Box.Text = ConvertToBase(Value, NumericBase.Base8);
                Base2Box.Text = ConvertToBase(Value, NumericBase.Base2);
            }
            catch (Exception ex)
            {
                Base10Box.Text = "";
                Base8Box.Text = "";
                Base2Box.Text = "";
            }
        }

        private void Base10Box_KeyUp(object sender, KeyEventArgs e)
        {
            string Input = Base10Box.Text;
            try
            {
                long Value = Convert.ToInt64(Input);
                Base16Box.Text = ConvertToBase(Value, NumericBase.Base16);
                Base8Box.Text = ConvertToBase(Value, NumericBase.Base8);
                Base2Box.Text = ConvertToBase(Value, NumericBase.Base2);
            }
            catch (Exception ex)
            {
                Base16Box.Text = "";
                Base8Box.Text = "";
                Base2Box.Text = "";
            }
        }

        private void Base8Box_KeyUp(object sender, KeyEventArgs e)
        {
            string Input = Base8Box.Text;
            try {
                Int64 Value = Convert.ToInt64(Input, 8);
                Base16Box.Text = ConvertToBase(Value, NumericBase.Base16);
                Base10Box.Text = ConvertToBase(Value, NumericBase.Base10);
                Base2Box.Text = ConvertToBase(Value, NumericBase.Base2);
            }
            catch (Exception ex)
            {
                Base16Box.Text = "";
                Base10Box.Text = "";
                Base2Box.Text = "";
            }
        }

        private void Base2Box_KeyUp(object sender, KeyEventArgs e)
        {
            string Input = Base2Box.Text;
            try {
                Int64 Value = Convert.ToInt64(Input, 2);
                Base16Box.Text = ConvertToBase(Value, NumericBase.Base16);
                Base10Box.Text = ConvertToBase(Value, NumericBase.Base10);
                Base8Box.Text = ConvertToBase(Value, NumericBase.Base8);
            }
            catch (Exception ex)
            {
                Base16Box.Text = "";
                Base10Box.Text = "";
                Base8Box.Text = "";
            }
        }


    }
}
