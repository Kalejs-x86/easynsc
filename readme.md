# Easy Numeral System Converter
A tool for easily converting between numeral systems on the fly. Very useful for reverse engineering when you don't have the patience to look up numeral system converters online.

![Screenshot](img/screen.png)

(Supports numbers up to **64-bit**!)

[Download](https://bitbucket.org/Kalejs-x86/easynsc/downloads/EasyNSC.exe)

# License
This project is licensed under the MIT license